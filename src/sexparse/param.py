#Copyright (c) 2014, Joseph Hunkeler <jhunkeler at gmail.com>
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import os
import collections
from sexparse.base import SexConfigBase
from sexparse.functions import *

__all__ = ['SexParam']

class SexParam(SexConfigBase):
    def __init__(self, filename):
        SexConfigBase.__init__(self, filename)
        self.status = []
        self.lparenthesis = '('
        self.rparenthesis = ')'
        self.read()
        
    def is_enabled(self, key):
        for status, keyword in zip(self.status, self.pairs.iterkeys()):
            if key == keyword:
                return status
    
    def enable(self, key, state):
        status_new = []
        mapping = zip(self.status, self.pairs.iterkeys())
        
        for status, keyword in mapping:
            if key == keyword:
                status = state
            status_new.append(status)
        
        self.status = status_new

    def align_values(self):
        pass

    def read(self):
        for index, line in enumerate(open(self.filename)):
            open_position = line.find(self.lparenthesis)
            close_position = line.find(self.rparenthesis)
            key = line[0:open_position]

            # empty lines don't matter to us
            if not key:
                continue

            # how about that syntax? did anyone screw up the file?
            if " " in key or "," in key:
                raise SyntaxError('{}:{}:malformed key\n{}'.format(self.filename, index+1, line))

            if open_position != -1 or close_position != -1:
                # we found a so-called valid item
                item = line[open_position+1:close_position].split(',')
                
                if not item:
                    continue
                
                item = [ x.strip(' ') for x in item ]
                item = [ convert_type(x) for x in item ]
                
                if len(item) == 1:
                    item = item[0]
                
                # we need to take a close look.
                if open_position < 0 or close_position < 0:
                    raise SyntaxError('{}:{}:malformed input item\n{}'.format(self.filename, index+1, line))
            else:
                # legitimately found nothing
                item = ''
            
            # strip comments for dictionary key values but keep track of them
            if key.startswith(self.comment):
                key = key.strip(self.comment)
                self.status.append(False)
            else:
                self.status.append(True)
                
            self.pairs[key] = item
        
    def write(self, filename=None):
        if filename is None:
            filename = self.filename
        outfile = open(filename, 'w+')
        for key, item in self.pairs.iteritems():
            if not self.is_enabled(key):
                key = self.comment + key
            if item:
                if isinstance(item, list):
                    item = self.lparenthesis + ",".join(map(str, item)) + self.rparenthesis   
                else:
                    item = self.lparenthesis + str(item) + self.rparenthesis
                
            outfile.write("{}{}{}".format(key, item, os.linesep))
        outfile.close()

