#Copyright (c) 2014, Joseph Hunkeler <jhunkeler at gmail.com>
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import os
from sexparse.base import SexConfigBase
from sexparse.functions import *

__all__ = ['SexConfig']


class SexConfig(SexConfigBase):
    def __init__(self, filename):
        SexConfigBase.__init__(self, filename)
        self.read()      
    
    def read(self):
        for line in open(self.filename):
            if line.startswith(self.comment):
                continue
            comment_position = line.find(self.comment)
            line = line[0:comment_position]
            key = line[0:line.find(' ')]
            
            if not key:
                continue
                        
            item = line[line.find(' '):].split(',')
            
            if not item:
                continue
            
            item = [ x.strip(' ') for x in item ]
            item = [ convert_type(x) for x in item ]
            
            if len(item) == 1:
                item = item[0]

            self.pairs[key] = item
    
    def write(self, filename=None):
        if filename is None:
            filename = self.filename
        outfile = open(filename, 'w+')
        for key, value in self.pairs.iteritems():
            outfile.write("{}\t{}{}".format(key, value, os.linesep))
        outfile.close()
