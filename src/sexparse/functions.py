#Copyright (c) 2014, Joseph Hunkeler <jhunkeler at gmail.com>
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import sexparse


def sexdump(obj, col_width=16, truncate=True):
    width = col_width 
    trunc = "<trunc>"
    if not isinstance(obj, sexparse.SexConfigBase):
        return None

    for k, v in obj:
        retainer = v
        if truncate:
            if len(repr(v)) > width:
                v = repr(v)[0:width - len(trunc)] + trunc

        show_type = type(retainer)
        sformat = "{:>{width}} | {:<{width}} | {:<{width}}"    
        sformat_t = (k, v, show_type)

        try:
            if obj.status:
                status = "Enabled" if convert_type(obj.is_enabled(k)) else "Disabled"
                sformat += " | {:>{width}}"
                sformat_t = (k, v, status, show_type)
        except AttributeError:
            pass

        if isinstance(v, list) or isinstance(v, tuple):
            show_type = [ type(x) for x in retainer ]

        if isinstance(v, dict):
            show_type = [ type(x) for x in retainer.iteritems() ]

        print(sformat.format(*sformat_t, width=col_width))


def convert_type(s):

    for cast in (int, float):
        try:
            return cast(s)
        except ValueError:
            pass
    return s
