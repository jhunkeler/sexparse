#!/usr/bin/env python

#Copyright (c) 2014, Joseph Hunkeler <jhunkeler at gmail.com>
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import os
import sexparse


DATA = os.path.abspath('data')


config = sexparse.SexConfig(os.path.join(DATA, 'default.sex'))

param = sexparse.SexParam(os.path.join(DATA, 'default.param'))
param.write(os.path.join(DATA, 'user.param'))

param_user = sexparse.SexParam(os.path.join(DATA, 'user.param'))
param_user['FLUX_APER'] = 2
param_user.enable('FLUX_APER', True)
param_user['VIGNET'] = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
param_user.write()


configurations = [ config, param, param_user ]
for obj in configurations:
    marks = len(obj.filename)
    
    print("#" * marks)
    print("{}".format(obj.filename))
    print("#" * marks)
    
    sexparse.sexdump(obj)
